/* globals Request */
'use strict';

const cacheName = 'sp_cache_v1',

    assets = [ 
        '/',
        'index.html',
        'style.min.css',
        'scripts/gif.js',
        'jquery-3.1.0.min.js',
        'flickity.pkgd.min.js',
        'script.min.js',
        'images/icons/arrow.svg',
        'images/icons/close.svg',
        'images/icons/exit-fs.svg',
        'images/icons/fullscreen.svg',
        'images/icons/options.svg',
        'images/icons/pause-red.svg',
        'images/icons/pause.svg',
        'images/icons/search.svg'
    ];

self.addEventListener('install', event => {
    console.info('Event: install service worker');
    event.waitUntil(caches.open(cacheName).then(cache => cache.addAll(assets)));
});

self.addEventListener('activate', event => {
    console.info('Event: activate service worker');
    event.waitUntil(caches.keys().then(keys => {
        return Promise.all(keys.map(key => {
            if (key !== cacheName) {
                console.info('Deleting old cache: ' + key);
                return caches.delete(key);
            }
        }));
    }).then(self.clients.claim()));
});

self.addEventListener('fetch', event => {
    var request = event.request;
    console.info('Event: fetch ' + request.url);
    if (request.url.includes('unavailable')) {
        console.warn('Picture unavailable');
        request = new Request('https://slick.pics/images/gifs/coffee.gif');
    }
    event.waitUntil(event.respondWith(caches.open(cacheName).then(cache => {
        return cache.match(request).then(cachedResponse => {
            cachedResponse && console.info('Asset found in cache');
            return cachedResponse || fetch(request).then(serverResponse => {
                console.info('Asset fetched from server');
                const isGif = request.url.includes('//slick.pics/images/gifs/');
                return isGif ? cache.put(request, serverResponse.clone()).then(() => {
                    console.info('Asset added to cache');
                    return serverResponse;
                }) : serverResponse;
            });
        });
    })));
});
