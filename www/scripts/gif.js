'use strict';

const gif = {
    portrait: [
        [0.67, 'owl',         'center',        'Snowy Owl'],
        [0.72, 'barber',      'center',        'Barber'],
        [0.72, 'beer',        'center',        'Beer'],
        [0.75, 'sail',        'center',        'Sail Boat'],
        [0.83, 'wine',        'left',          'Wine']
    ],

    landscape: [
        [1.31, 'home',        'center',        ''],
        [1.40, 'balcony',     'top right',     ''],
        [1.56, 'cat',         'right',         'White Cat'],
        [1.56, 'strangelove', 'center',        'Dr. Strangelove'],
        [1.62, 'stir',        'center',        'Stir the Coffee'],
        [1.67, 'projector',   'center',        'Film Projector'],
        [1.68, 'fireplace',   'center',        'Fireplace'],
        [1.78, 'bicycle',     'bottom middle', 'Vintage Bicycle'],
        [1.78, 'coffee',      'top left',      'Coffee Machine'],
        [1.78, 'girl',        'top left',      ''],
        [1.78, 'globe',       'right',         'Spinning Globe'],
        [1.78, 'swing',       'left',          ''],
        [1.78, 'wristwatch',  'bottom',        ''],
        [1.93, 'subway',      'bottom right',  ''],
        [2.12, 'tomatoes',    'right',         'Fresh Tomatoes'],
        [2.40, '1920s',       'center',        '1920s Times Square'],
        [2.34, 'dune',        'center',        'Dune']
    ]
};

(() => {
    const windowRatio = window.innerWidth / window.innerHeight,
        orientation = windowRatio < 1.1 ? 'portrait' : 'landscape',
        imageIndex = Math.floor(Math.random() * (gif[orientation].length)), 
        src = `images/gifs/${gif[orientation][imageIndex][1]}.gif`,
        pos = gif[orientation][imageIndex][2],
        attr = gif[orientation][imageIndex][3];
    document.write(`<style id="gif-style" data-attribution="${attr}">#pic-1 { background-image: url(${src}); background-position: ${pos}; }</style>`);
})();
