/* globals $, state, elem, util, a500px, flickr, local */
'use strict';

const sp = {

    getFullscreenElem() {
        //console.log('sp.getFullscreenElem()');
        var elem = document.fullscreenElement;
        $.each(['webkit', 'moz', 'ms'], (i, prefix) => {
            if (typeof document[prefix + 'FullscreenElement'] !== 'undefined') {
                elem = document[prefix + 'FullscreenElement'];
                return false;
            }
        });
        return elem;
    },

    showCtrls() {
        //console.log('sp.showCtrls()');
        clearTimeout(state.ctrlsTimeout);
        const showPicInfo = (elem.opts.picInfo.show.val() === 'With Controls');
        state.ctrlsTimeout = setTimeout(() => {
            elem.layers.ctrls.removeClass('trans-faster show');
            showPicInfo && sp.setPicInfoOverlay('hide');
        }, elem.opts.ctrls.hide.val() * 1000);
        if (!elem.layers.ctrls.hasClass('show')) {
            elem.layers.ctrls.addClass('trans-faster show');
            showPicInfo && sp.setPicInfoOverlay('show');
        }
    },

    sanitizeInput(input = elem.opts.filter.tags) {
        console.log(`sp.sanitizeInput(${input.attr('id')})`);
        let originalVal = input.val(),
            sanitizedVal = util.removeHtmlTags(originalVal);
        (sanitizedVal === originalVal) || input.val(sanitizedVal);
    },

    cleanTags(input = elem.opts.filter.tags) {
        //console.log(`sp.cleanTags(${input.attr('id')})`);
        const unsafeWords = ['arse', 'beastiality', 'blowjob', 'boob', 'breast',
            'cunt', 'death', 'defecat', 'fuck', 'hentai', 'immolat', 'kill',
            'murder', 'naked', 'necrophilia', 'nude', 'nudity', 'orgasm', 'orgy',
            'pedophil', 'penis', 'poo', 'porn', 'pussy', 'rape', 'sex', 'shit',
            'slaughter', 'suicide', 'under age', 'underage', 'vagina', 'violence'],
            originalTags = input.val();
        let cleanedTags = originalTags;

        $.each(unsafeWords, (i, unsafeWord) => {
            cleanedTags = cleanedTags.replace(unsafeWord, '');
        });
        if (cleanedTags !== originalTags) {
            input.val(cleanedTags);
        }
    },

    setPicInfoOverlay(value = elem.opts.picInfo.show.val()) {
        console.log(`sp.setPicInfoOverlay(${value})`);
        const changeClass = (value === 'show' || value === 'Always') ? 'addClass' : 'removeClass';
        elem.picInfo.pic1Overlay[changeClass]('show');
        elem.picInfo.pic2Overlay[changeClass]('show');
        elem.picInfo.pic3Overlay[changeClass]('show');
    },

    hideOpts() {
        console.log('sp.hideOpts()');
        elem.layers.opts.removeClass('show');
        elem.layers.pic1.removeClass('blur');
        elem.layers.pic2.removeClass('blur');
        elem.layers.pic3.removeClass('blur');
        if (elem.opts.picInfo.show.val() === 'Always') {
            sp.setPicInfoOverlay('Always');
        }
        state.isPaused || sp.setPicInterval();
        document.addEventListener('mousemove', sp.showCtrls);
    },

    showOpts() {
        console.log('sp.showOpts()');
        document.removeEventListener('mousemove', sp.showCtrls);
        clearTimeout(state.ctrlsTimeout);
        clearTimeout(state.refreshTimeout);
        clearInterval(state.changeInterval);
        sp.setPicInfoOverlay('hide');
        elem.layers.ctrls.removeClass('show');
        elem.layers.pic1.addClass('blur');
        elem.layers.pic2.addClass('blur');
        elem.layers.pic3.addClass('blur');
        elem.layers.opts.addClass('show');
    },

    setFileList(files = elem.opts.sources.files[0].files) {
        //console.log(`sp.setFileList(${files})`);
        let text = '';
        if (files && files.length > 1) {
            text = files.length + ' files chosen';
        } else if (!files || files.length < 1) {
            text = 'Choose files...';
        } else {
            const fileNames = Array.from(files).map(file => file.name);
            fileNames && (text = fileNames.join(', '));
        }
        elem.opts.sources.filelist.val(text);
    },

    getRndItemIndex() {
        //console.log('sp.getRndItemIndex()');
        return util.getRndInt(0, state.unshownItems.length - 1);
    },

    getPicLayerIndex() {
        //console.log('sp.getPicLayerIndex()');
        return elem.layers.pic1.hasClass('show') ? 1 : (elem.layers.pic2.hasClass('show') ? 2 : 3);
    },

    prevPicLayerIndex(layerIndex = sp.getPicLayerIndex()) {
        console.log(`sp.prevPicLayerIndex(${layerIndex})`);
        return (layerIndex === 1) ? 3 : layerIndex - 1;
    },

    nextPicLayerIndex(layerIndex = sp.getPicLayerIndex()) {
        //console.log(`sp.nextPicLayerIndex(${layerIndex})`);
        return (layerIndex === 3) ? 1 : layerIndex + 1;
    },

    changePic(pos, changeMin = elem.opts.display.change.val()) {
        if (typeof pos === 'object' && pos.target) {
            pos = pos.target.id;
        }
        console.log(`sp.changePic(${pos}, ${changeMin})`);
        if (state.doClearItems) {
            state.doClearItems = false;
            sp.setNextPic(sp.getRndItemIndex());
        }
        if (state.unshownItems.length) {
            // Fetch more items if 10+ min have passed since last change
            // so that displayed pictures are more random.
            // BUG: This reduces changeInterval by ~0.5 min so if fetch
            // takes longer than 0.5 min the pic will be changed twice
            // because processItems currently calls showPic.
            if (!state.shownIndex && changeMin >= 10) {
                clearTimeout(sp.refreshTimeout);
                state.refreshTimeout = setTimeout(() => {
                    sp.fetchItems();
                }, (changeMin - 0.5) * 60000);
            }
            pos === 'prev' ? sp.showPrevPic() : sp.showNextPic();
        } else if (state.shownItems.length) {
            state.fetchTimeout = setTimeout(() => {
                !state.isPaused && sp.fetchItems();
            }, 10000);
            sp.reshowPic();
        } else {
            !state.isPaused && sp.fetchItems();
        }
    },

    transition(current, next) {
        console.log(`sp.transition(${current}, ${next})`);
        elem.layers['pic' + current].removeClass('show');
        elem.layers['pic' + next].addClass('show');
    },

    getPicSrc(item) {
        //console.log(`sp.getPicSrc(${item})`);
        return window[item.source].getPicSrc(item);
    },

    setFirstPic(rnd = sp.getRndItemIndex()) {
        console.log(`sp.setFirstPic(${rnd})`);
        const src = sp.getPicSrc(state.unshownItems[rnd]);
        elem.layers.pic2.css('background-image', `url(${src})`);
        state.shownItems.push(state.unshownItems[rnd]);
        state.unshownItems.splice(rnd, 1);
    },

    setPrevPic() {
        console.log('sp.setPrevPic()');
        const item = state.shownItems[state.shownItems.length + state.shownIndex - 3],
            src = sp.getPicSrc(item),
            layerIndex = sp.prevPicLayerIndex();
        console.info('item.title: ' + item.title);
        elem.layers['pic' + layerIndex].css('background-image', `url(${src})`);
        sp.setPicInfo(layerIndex, item);
    },

    setNextPic(itemIndex = sp.getRndItemIndex()) {
        console.log(`sp.setNextPic(${itemIndex})`);
        const item = state.shownIndex ? state.shownItems[state.shownItems.length + state.shownIndex - 1] : state.unshownItems[itemIndex],
            src = sp.getPicSrc(item),
            layerIndex = sp.nextPicLayerIndex();
        console.info('item.title: ' + item.title);
        elem.layers['pic' + layerIndex].css('background-image', `url(${src})`);
        sp.setPicInfo(layerIndex, item);
    },

    setPicInfoVisibility(layerIndex = sp.getPicLayerIndex(), picInfo = elem.picInfo['pic' + layerIndex], {
        title  = picInfo.title.html(),
        author = picInfo.author.html(),
        tags   = picInfo.tags.html(),
        date   = picInfo.date.html()
    } = {}) {
        //console.log('sp.setPicInfoVisibility()');
        const info = { title, author, tags, date };
        let showTitleAuthor = false;

        for (let item of Object.keys(info)) {
            if (elem.opts.picInfo[item]) {
                if (info[item] && elem.opts.picInfo[item].prop('checked')) {
                    picInfo[item].removeClass('hide');
                    if (item === 'title' || item === 'author') {
                        showTitleAuthor = true;
                    }
                } else {
                    picInfo[item].addClass('hide');
                }
            }
        }
        elem.picInfo['pic' + layerIndex].titleAuthor[showTitleAuthor ? 'removeClass' : 'addClass']('hide');
    },

    setPicInfo(layerIndex = sp.getPicLayerIndex(),
        item = state.shownItems[state.shownItems.length - 2] || state.shownItems[0] || {}) {
        //console.log(`sp.setPicInfo(${item})`);
        const picInfo = elem.picInfo['pic' + layerIndex];
        picInfo.title.html(item.title ? util.removeHtmlTags(item.title) : '');
        picInfo.author.html(item.author ? 'by ' + util.removeHtmlTags(item.author) : '');
        picInfo.tags.html(item.tags ? util.removeHtmlTags(item.tags.substring(0, window.innerWidth / 4).replace(/\w+$/, '')) : '');
        picInfo.date.html(item.date ? util.formatDate(item.date) : '');
        const wrapDate = (item.title && item.author) || (item.title && item.tags) || (item.author && item.tags);
        picInfo.date[wrapDate ? 'addClass' : 'removeClass']('wrap');
        sp.setPicInfoVisibility(layerIndex, picInfo, item);
    },

    reshowPic() {
        console.log('sp.reshowPic()');
        state.shownItems.push(state.shownItems.shift());
        const src = sp.getPicSrc(state.shownItems[state.shownItems.length - 1]);
        elem.layers['pic' + sp.getPicLayerIndex()].css('background-image', `url(${src})`);
    },

    showPrevPic() {
        console.log('sp.showPrevPic()');
        const layerIndex = sp.getPicLayerIndex();
        sp.transition(layerIndex, sp.prevPicLayerIndex(layerIndex));
        if (Math.abs(--state.shownIndex) + 2 === state.shownItems.length) {
            elem.ctrls.prev.addClass('disabled');
        } else {
            sp.setPrevPic();
        }
    },

    showNextPic() {
        //console.log('sp.showNextPic()');
        const layerIndex = sp.getPicLayerIndex();
        sp.transition(layerIndex, sp.nextPicLayerIndex(layerIndex));
        setTimeout(sp.setPicInfo, 10);

        // Remove style for intro cinemagraph
        if (elem.gifStyle && layerIndex === 1) {
            setTimeout(() => {
                elem.gifStyle.remove();
                delete elem.gifStyle;
            }, 1000);
        }
        state.shownIndex < 0 && state.shownIndex++;
        if (state.shownIndex) {
            sp.setNextPic();
        } else {
            const itemIndex = sp.getRndItemIndex();
            sp.setNextPic(itemIndex);
            state.shownItems.push(state.unshownItems[itemIndex]);
            state.unshownItems.splice(itemIndex, 1);
        }
        elem.ctrls.prev.removeClass('disabled');
    },

    setPicInterval() {
        console.log('sp.setPicInterval()');
        clearInterval(state.changeInterval);
        const changeMin = elem.opts.display.change.val();
        if (changeMin > 0) {
            state.changeInterval = setInterval(() => {
                sp.changePic('next', changeMin);
            }, changeMin * 60000);
        }
    },

    showPics() {
        console.log('sp.showPics()');
        if (state.unshownItems.length) {
            if (state.isInit) {
                state.isInit = false;
                sp.setFirstPic();
            } else {
                sp.changePic('next');  //sp.showNextPic();
            }
        }
        elem.layers.opts.hasClass('show') || sp.setPicInterval();
    },

    fetchItems(source) {
        console.log(`sp.fetchItems(${source})`);
        clearTimeout(state.fetchTimeout);
        switch (source) {
            case 'a500px':
                a500px.fetchItems();
                break;
            case 'flickr':
                flickr.fetchItems();
                break;
            case 'local':
                local.processItems(elem.opts.sources.files[0].files);
                break;
            default:
                elem.opts.sources.buttons[0].classList.contains('selected') && flickr.fetchItems();
                elem.opts.sources.buttons[1].classList.contains('selected') && a500px.fetchItems();
                elem.opts.sources.buttons[2].classList.contains('selected') && elem.opts.sources.files[0].files && local.processItems(elem.opts.sources.files[0].files);
        }
    },

    removeItems(source, doShown) {
        console.log(`sp.removeItems(${source}, ${doShown})`);
        const isNotSource = item => item.source !== source;
        state.unshownItems = state.unshownItems.filter(isNotSource);
        doShown && (state.shownItems = state.shownItems.filter(isNotSource));
    }

};
