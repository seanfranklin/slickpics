/* globals $, state */
'use strict';

var local = {

    getPicSrc(item) {
        //console.log(`local.getPicSrc(${item})`);
        //return new FileReaderSync().readAsDataURL(item);
        return item.src;
    },

    restructureItem(item) {
        //console.log(`local.restructureItem(${item})`);
        return {
            source: 'local',
            title:  item.name,
            date:   item.lastModified,
            src:    window.URL.createObjectURL(item)
        };
    },

    processItems(items) {
        console.log(`local.processItems(${items})`);
        if (items) {
            items = $.makeArray(items).filter(item => item.type.match('image.*'))
                .map(item => local.restructureItem(item));
        }
        if (state.doClearItems) {
            state.shownItems = [];
            state.unshownItems = items;
        } else {
            items && $.merge(state.unshownItems, items);
        }
    }
};
