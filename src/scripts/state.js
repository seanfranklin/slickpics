'use strict';

const state = {

    shownIndex:        0,      // Reverse index of current item position in shownItems
    shownItems:        [],
    unshownItems:      [],

    changeInterval:    null,
    pauseInterval:     null,

    ctrlsTimeout:      null,
    fetchTimeout:      null,
    refreshTimeout:    null,

    doClearItems:      false,  // Clear shownItems and unshownItems when new items are fetched
    isPaused:          false,
    isAuthed:          false,
    isInit:            true,
    isFileApi:         !!(window.File && window.FileReader && window.FileList && window.Blob),

    requestFullscreen: document.body.requestFullscreen || document.body.webkitRequestFullscreen || document.body.mozRequestFullscreen || document.body.msRequestFullscreen,
    exitFullscreen:    document.exitFullscreen || document.webkitExitFullscreen || document.mozExitFullscreen || document.msExitFullscreen

};
