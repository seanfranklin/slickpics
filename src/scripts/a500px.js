/* globals $, geo, sp, state, elem */
'use strict';

var a500px = {

    isAuthed:     false,

    rest: {
        endpoint: 'https://api.500px.com/v1/photos/search',
        query:    '?image_size=1,2,3,4,5,30,100,200,440,600,1080,1600,2048&tags=1&term='
    },

    getPicSrc(item) {
        //console.log(`flickr.getPicSrc(${item})`);
        const vw = window.innerWidth,
            vh = window.innerHeight,
            vmin = vw < vh ? vw : vh;
        let size = 2048;  // 2048px on max edge

        if (vw <= 70 && vh <= 70) {
            size = 1;     // 70x70
        } else if (vw <= 100 && vh <= 100) {
            size = 100;   // 100x100
        } else if (vw <= 140 && vh <= 140) {
            size = 2;     // 140x140
        } else if (vw <= 200 && vh <= 200) {
            size = 200;   // 200x200
        } else if (vw <= 280 && vh <= 280) {
            size = 3;     // 280x280
        } else if (vmin <= 256) {
            size = 30;    // 256px on max edge
        } else if (vw <= 440 && vh <= 440) {
            size = 440;   // 440x440
        } else if (vw <= 600 && vh <= 600) {
            size = 600;   // 600x600
        } else if (vmin <= 900) {
            size = 4;     // 900px on max edge
        } else if (vmin <= 1080) {
            size = 1080;  // 1080px on max edge
        } else if (vmin <= 1170) {
            size = 5;     // 1170px on max edge
        } else if (vmin <= 1600) {
            size = 1600;  // 1600px on max edge
        }
        for (let sizeObj of item.sizes) {
            if (sizeObj.size === size) {
                return sizeObj.src;
            }
        }
        // Return default src if failed to match size
        return item.src;
    },    

    restructureItem({ name, user: { username }, created_at, tags, image_url, images }) {
        //console.log('a500px.restructureItem()');
        images = images.map(({ size, url }) => ({ size, src: url }));
        return {
            source: 'a500px',
            title:  name,
            author: username,
            date:   created_at,
            tags:   tags.join(' '),
            src:    image_url,
            sizes:  images
        };
    },

    processItems(json) {
        console.log(`a500px.processItems(${json})`);
        if (json.photos) {
            console.info(`Fetched ${json.photos.length} items`);
            const isSafe = elem.opts.filter.safe.hasClass('selected'),
                items = json.photos.filter(item => isSafe ? !item.nsfw : true)
                    .map(item => a500px.restructureItem(item));
            if (state.doClearItems) {
                state.shownItems = [];
                state.unshownItems = items;
            } else {
                $.merge(state.unshownItems, items);
            }
        } else {
            console.warn('No items fetched from 500px!');
        }
        state.isStopped || sp.showPics();
    },

    createData(tags = elem.opts.filter.tags.val(), place = elem.opts.filter.geo.val()) {
        //console.log(`a500px.createData(${tags}, ${place})`);
        let data = {
            consumer_key: a500px.key,
            tags: 1
        };
        if (place) {
            let location;
            if (a500px.isAuthed) {
                location = geo ? geo[place] : null;
                if (location.latitude && location.longitude) {
                    data.geo = `${location.latitude},${location.longitude},50km`;
                } else {
                    location = null;
                }
            }
            if (!location || !a500px.isAuthed) {
                tags += ',' + place;
            }
        }
        data.term = tags;
        return data;
    },

    createUrl(tags = elem.opts.filter.tags.val(), place = elem.opts.filter.geo.val()) {
        //console.log(`a500px.createUrl(${tags}, ${place})`);
        if (place) {
            let location;
            if (a500px.isAuthed) {
                location = geo ? geo[place] : null;
                if (location.latitude && location.longitude) {
                    tags += `&geo=${location.latitude},${location.longitude},50km`;
                } else {
                    location = null;
                }
            }
            if (!location || !a500px.isAuthed) {
                tags += ',' + place;
            }
        }
        return a500px.rest.endpoint + a500px.rest.query + tags + '&consumer_key=' + a500px.key;
    },

    fetchItems() {
        console.log('a500px.fetchItems()');
        $.get(a500px.createUrl()).then(a500px.processItems);
    }

};
