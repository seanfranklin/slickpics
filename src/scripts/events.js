/* globals $, sp, elem, state, local */
'use strict';

/**
 *  Event listeners to support local files.
 */

if (state.isFileApi) {
    document.addEventListener('dragover', event => {
        event.preventDefault();
        event.stopPropagation();
        event.dataTransfer.dropEffect = 'copy';
    });

    document.addEventListener('drop', event => {
        console.info('Event: drop on document');
        event.preventDefault();
        event.stopPropagation();
        const droppedFiles = event.dataTransfer.files,
            existingFiles = elem.opts.sources.files[0].files;
        /* if (existingFiles) {
            $.merge(existingFiles, droppedFiles);
        } else { */
            elem.opts.sources.files[0].files = droppedFiles;
        // }
        sp.setFileList();
        elem.opts.sources.files.hasClass('selected') && local.processItems(droppedFiles);
    });

    elem.opts.sources.browse.click(event => {
        console.info('Event: click on sources-browse');
        elem.opts.sources.files.click();
    });

    elem.opts.sources.files.change(event => {
        console.info('Event: sources-files changed');
        sp.setFileList();
        sp.removeItems('local');
        local.processItems(event.target.files);
    });
}


/**
 *  Event listeners for top controls.
 */

elem.ctrls.search.click(() => {
    console.info('Event: click on search');
    elem.ctrls.searchTags.focus();
    elem.ctrls.searchTags.select();
});

elem.ctrls.searchTags.change(() => {
    console.info('Event: search-tags changed');
    sp.sanitizeInput(elem.ctrls.searchTags);
    elem.ctrls.searchTags.blur();
    state.doClearItems = true;
    sp.fetchItems();
});

elem.ctrls.searchTags.blur(() => {
    console.info('Event: blur on search-tags');
    sp.showCtrls();
    document.addEventListener('mousemove', sp.showCtrls);
});

elem.ctrls.searchTags.focus(() => {
    console.info('Event: focus on search-tags');
    document.removeEventListener('mousemove', sp.showCtrls);
    clearTimeout(state.ctrlsTimeout);
});

elem.ctrls.searchTags.on('input', () => {
    elem.opts.filter.safe.hasClass('selected') && sp.cleanTags(elem.ctrls.searchTags);
    elem.opts.filter.tags.val(elem.ctrls.searchTags.val());
});

elem.ctrls.opts.click(sp.showOpts);

elem.ctrls.fullscreen.click(() => {
    console.info('Event: click on fullscreen');
    if (sp.getFullscreenElem()) {
        state.exitFullscreen.call(document);
        elem.ctrls.fullscreen.css('background-image', 'url(images/icons/fullscreen.svg)');
    } else {
        state.requestFullscreen.call(document.body);
        elem.ctrls.fullscreen.css('background-image', 'url(images/icons/exit-fs.svg)');
    }
});


/**
 *  Event listeners for center controls.
 */

elem.ctrls.pause.click(() => {
    console.info('Event: click on pause');
    state.isPaused = !state.isPaused;
    state.isPaused ? clearInterval(state.changeInterval) : sp.setPicInterval();
    let icon = state.isPaused ? 'pause-red' : 'pause';
    elem.ctrls.pause.css('background-image' , `url(images/icons/${icon}.svg)`);
});

elem.ctrls.prev.click(() => {
    console.info('Event: click on prev');
    if (!elem.ctrls.prev.hasClass('disabled')) {
        sp.changePic('prev');
        state.isPaused || sp.setPicInterval();
    }
});

elem.ctrls.next.click(() => {
    console.info('Event: click on next');
    sp.changePic('next');
    state.isPaused || sp.setPicInterval();
});


/**
 *  Event listeners for options layer.
 */

elem.layers.opts.click(event => {
    event.target.classList.contains('js-close') && sp.hideOpts();
});

elem.layers.opts.on('select.flickity', () => {
    if (elem.opts.data) {
        console.info(`Event: ${elem.opts.data.selectedElement.id} selected`);
        elem.opts.tips.removeClass('show');
        elem.opts.tips[elem.opts.data.selectedIndex].classList.add('show');
    }
});


/**
 *  Event listeners for sources options.
 */

elem.opts.sources.buttons.click(event => {
    console.info('Event: click on ' + event.target.id);
    elem.opts.sources.buttons.each(function () {
        if (this.id === event.target.id) {
            const source = this.id.substring(8, this.id.length),
                button = $(this),
                isSelected = button.hasClass('selected');
            button.toggleClass('selected');
            sp[isSelected ? 'removeItems' : 'fetchItems'](source);
        }
        /* Uncomment to disable multiple sources
        else {
            $(this).removeClass('selected');
        } */
    });
});


/**
 *  Event listeners for filter options.
 */

for (let input of Object.keys(elem.opts.filter)) {
    elem.opts.filter[input].focus(function () {
        console.info(`Event: focus on ${this.id}`);
        this.select();
    });
    elem.opts.filter[input].change(function () {
        console.info(`Event: ${this.id} changed`);
        if (this.id === 'filter-tags' || this.id === 'filter-geo') {
            sp.sanitizeInput($(this));
        }
        state.doClearItems = true;
        sp.fetchItems();
    });
}

elem.opts.filter.tags.on('input', () => {
    elem.opts.filter.safe.hasClass('selected') && sp.cleanTags();
    elem.ctrls.searchTags.val(elem.opts.filter.tags.val());
});

elem.opts.filter.safe.click(() => {
    console.info('Event: click on filter-safe');
    elem.opts.filter.safe.toggleClass('selected');
    elem.opts.filter.safe.hasClass('selected') && sp.cleanTags();
});


/**
 *  Event listeners for display options.
 */

/* Only needed once Flickr auth is implemented
elem.opts.display.change.on('input', () => {
    console.info('Event: input on display-opts');
    const showTip = !state.isAuthed && elem.opts.display.change.val() < 30 && elem.opts.sources.buttons[0].hasClass('selected');
    $('#change-tip')[showTip ? 'removeClass' : 'addClass']('hide');
}); */

elem.opts.display.pause.change(() => {
    console.info('Event: display-pause changed');
    clearInterval(sp.pauseInterval);
    if (elem.opts.display.pause.val() > 0) {
        state.pauseInterval = setInterval(() => {
            var minRemaining = elem.opts.display.pause.val() - 1;
            console.info('minRemaining: ' + minRemaining);
            elem.opts.display.pause.val(minRemaining);
            if (minRemaining === 0) {
                clearInterval(state.pauseInterval);
                clearTimeout(state.refreshTimeout);
                clearInterval(state.changeInterval);
                elem.layers['pic' + sp.getPicLayerIndex()].css('background-image', 'unset');
                state.isPaused = true;
                elem.ctrls.pause.css('background-image' , `url(images/icons/pause-red.svg)`);
            }
        }, 60000);
    } else {
        state.isPaused = false;
        elem.ctrls.pause.css('background-image' , `url(images/icons/pause.svg)`);
    }
});


/**
 *  Event listeners for other options.
 */

elem.opts.ctrls.position.change(() => {
    console.info('Event: ctrls-position changed');
    elem.layers.ctrls.css('justify-content', elem.opts.ctrls.position.val());
});

for (let checkbox of Object.keys(elem.opts.picInfo)) {
    elem.opts.picInfo[checkbox].change(() => {
        sp.setPicInfoVisibility(1, elem.picInfo.pic1);
        sp.setPicInfoVisibility(2, elem.picInfo.pic2);
        sp.setPicInfoVisibility(3, elem.picInfo.pic3);
    });
}


/**
 *  Additional event listeners for document and window.
 */

document.addEventListener('mousemove', sp.showCtrls);

if (typeof document.hidden !== 'undefined') {
    document.addEventListener('visibilitychange', () => {
        console.info('Event: document visibilitychange');
        if (document.hidden) {
            clearTimeout(state.refreshTimeout);
            clearInterval(state.changeInterval);
        } else {
            state.isPaused || elem.layers.opts.hasClass('show') || sp.setPicInterval();
        }
    });
}

window.onresize = () => {
    console.info('Event: resize window');
    if (state.requestFullscreen && sp.getFullscreenElem() === null) {
        elem.ctrls.fullscreen.removeClass('hide');
    }
};
