/* globals sp, elem, state, util */
'use strict';

/**
 *  Flickity carousel for the options layer and its navigation.
 */

elem.layers.opts.flickity({
    arrowShape: 'M 0,50 L 50,00 L 50,30 L 80,30 L 80,70 L 50,70 L 50,100 Z',  // SVG path string for left arrow in 100x100 box
    accessibility:        true,         // Keyboard navigation with tab and arrow keys
    asNavFor:             false,        // Element or selector of another carousel to navigate for
    autoPlay:             false,        // Automatically advance to next cell after number of milliseconds
    cellAlign:            'center',     // Align cells within the carousel element
    cellSelector:         'fieldset',   // Selector for cell elements
    contain:              false,        // Contain cells to carousel to prevent excess scroll at beginning/end
    draggable:            true,         // Enable dragging and flicking via mouse/touch
    freeScroll:           false,        // Scroll and flick without aligning cells to an end position
    freeScrollFriction:   0.075,        // Affects movement when freeScroll is true
    friction:             0.28,         // Higher feels stickier and bouncy while lower feels looser and wobbly
    initialIndex:         0,            // Zero-based index of the initial selected cell
    pageDots:             false,        // Create and enable page dots
    pauseAutoPlayOnHover: false,        // Pause auto-playwhen the user hovers over carousel
    percentPosition:      true,         // Set cell positioning in percent values rather than pixels
    prevNextButtons:      false,        // Create and enable previous and next buttons
    resize:               true,         // Adjust sizes and positions when window is resized
    selectedAttraction:   0.025,        // Attraction/speed of the slider's position to the selected cell
    setGallerySize:       true,         // Set carousel height from tallest cell rather than CSS
    watchCSS:             true,         // Enable/disable carousel by watching content of ::after
    wrapAround:           false         // Wrap-around to beginning for infinite scrolling
});

elem.opts.nav.flickity({
    asNavFor:             '#opts',
    cellSelector:         '.nav-item',
    contain:              true,
    pageDots:             false,
    prevNextButtons:      false,
    watchCSS:             true
});

elem.opts.data = elem.layers.opts.data('flickity');

// Apply picInfo item visibility according to picInfo options
for (let key of Object.keys(elem.opts.picInfo)) {
    if (key !== 'show') {
        const isChecked = elem.opts.picInfo[key].prop('checked'),
            changeClass = isChecked ? 'removeClass' : 'addClass';
        elem.picInfo.pic1[key][changeClass]('hide');
        elem.picInfo.pic2[key][changeClass]('hide');
        elem.picInfo.pic3[key][changeClass]('hide');
    }
}

// Always show picInfo overlays if option is set
if (elem.opts.picInfo.show.val() === 'Always') {
    sp.setPicInfoOverlay('Always');
}

// Show fullscreen icon if supported and not already fullscreen
if (state.requestFullscreen && sp.getFullscreenElem() === null) {
    elem.ctrls.fullscreen.removeClass('hide');
}

// Show sources button and browse input for local files
if (state.isFileApi) {
    elem.opts.sources.buttons[2].classList.remove('hide');
    $('#files').removeClass('hide');
}

// Set random effects class on options navigation
elem.opts.nav.addClass(util.getRndInt() < 5 ? 'fx-in' : 'fx-up');

// Automatically fetch pic items when app starts
sp.fetchItems();

// Set picture info for cinemagraph
elem.picInfo.pic1.tags.addClass('hide');
elem.picInfo.pic1.author.html(elem.gifStyle.data('attribution'));

// Set pause button to white to indicate app has finished loading
elem.ctrls.pause.css('background-image' , 'url(/images/icons/pause.svg)');

// Register the service worker that intercepts network requests
if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('/sw.js').then(reg => {
        console.info('Service worker registered with scope:', reg.scope);
    }).catch(error => {
        console.error('Service worker not registered:', error);
    });
}
