/* globals $, geo, sp, state, elem */
'use strict';

var flickr = {

    isAuthed:     false,

    feed: {
        endpoint: 'https://api.flickr.com/services/feeds/photos_public.gne',
        query:    '?format=json&nojsoncallback=1&tags='
    },

    /* TODO: Add support for authenticated flickr.photos.search
    rest: {
        endpoint: 'https://api.flickr.com/services/rest/',
        query:    '?format=json&nojsoncallback=1&method=flickr.photos.search&sort=random&tags=&api_key='
    }, */

    getPicSrc(item) {
        //console.log(`flickr.getPicSrc(${item})`);
        const vw = window.innerWidth,
            vh = window.innerHeight;
        let size = 's';  // 75x75

        if (vw > 1024 || vh > 768) {
            size = 'h';  // 1600px wide
        } else if (vw > 800 || vh > 600) {
            size = 'b';  // 1024px wide
        } else if (vw > 640 || vh > 480) {
            size = 'c';  // 800px wide
        } else if (vw > 320 || vh > 240) {
            size = 'z';  // 640px wide
        } else if (vw > 240 || vh > 180) {
            size = 'n';  // 320px wide
        } else if (vw > 150 || vh > 100) {
            size = 'm';  // 240px wide
        } else if (vw > 100 || vh > 75) {
            size = 'q';  // 150x150
        } else if (vw > 75  || vh > 50) {
            size = 't';  // 100px wide
        }
        return item.src.replace('_m.jpg', `_${size}.jpg`);
    },

    restructureItem({ title, author, date_taken, tags, media: { m } }) {
        //console.log('flickr.restructureItem()');
        return {
            source: 'flickr',
            title,
            author: author.substring(19, author.length - 1),
            date:   date_taken,
            tags,
            src:    m
        };
    },

    processItems(json) {
        console.log(`flickr.processItems(${json})`);
        if (json.items) {
            console.info(`Fetched ${json.items.length} items`);
            const items = json.items.map(item => flickr.restructureItem(item));
            if (state.doClearItems) {
                state.shownItems = [];
                state.unshownItems = items;
            } else {
                $.merge(state.unshownItems, items);
            }
        } else {
            console.warn('No items fetched from Flickr!');
        }
        state.isPaused || sp.showPics();
    },

    createData(tags = elem.opts.filter.tags.val(), place = elem.opts.filter.geo.val()) {
        //console.log(`flickr.createData(${tags}, ${place})`);
        let data = { format: 'json' };
        if (place) {
            if (flickr.isAuthed) {
                data.api_key = flickr.key;
                let placeId = geo.getPlaceId(place);
                placeId && (data.place_id = placeId); 
            } else {
                tags += ',' + place;
            }
        }
        data.tags = tags;
        return data;
    },

    createUrl(tags = elem.opts.filter.tags.val(), place = elem.opts.filter.geo.val()) {
        //console.log(`flickr.createUrl(${tags}, ${place})`);
        if (place) {
            if (flickr.isAuthed) {
                const placeId = geo.getPlaceId(place);
                placeId && (tags += '&place_id=' + placeId);                
            } else {
                tags += ',' + place;
            }
        }
        const service = [flickr.isAuthed ? 'rest' : 'feed'];
        return flickr[service].endpoint + flickr[service].query + tags;
    },

    fetchItems() {
        console.log('flickr.fetchItems()');
        $.ajax({
            url: flickr[flickr.isAuthed ? 'rest' : 'feed'].endpoint,
            dataType: 'jsonp',
            data: flickr.createData()
        });
    },

    fetchPlaceData(place) {
        console.log(`flickr.fetchPlaceData(${place})`);
        $.ajax({
            url: flickr.rest.endpoint,
            dataType: 'jsonp',
            data: {
                api_key: flickr.key,
                format:  'json',
                method:  'flickr.places.find',
                query:   place
            }
        });
    }

};

/* TODO: Add Flickr user authentication via a node.js API proxy
function jsonFlickrApi(json) {
    console.log('jsonFlickrApi()');
    if (json.stat) {
        if (json.photos) {
            let rnd = Math.ceil(Math.random() * 100);
            let photo = json.photos.photo[rnd];
            let picUrl = `https://farm${photo.farm}.static.flickr.com/${photo.server}/${photo.id}_${photo.secret}_m.jpg`;
            let picLink = `https://www.flickr.com/photos/${photo.owner}/${photo.id}`;
        } else if (json.places) {
            json.places.place && flickr.places.push(json.places.place[0]);
        }
    }
} */


function jsonFlickrFeed(json) {
    flickr.processItems(json);
}
