'use strict';

var util = {

    /**
     *  Formats a date as DD-Mon-YYYY.
     *  @param {string} date - Unix date or timestamp
     *  @return {string} Formatted date
     */

    formatDate(date) {
        //console.log(`util.formatDate(${date})`);
        const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        date = new Date(date);
        return`${date.getDate()} ${months[date.getMonth()]} ${date.getFullYear()}`;
    },


    /**
     *  Picks a random integer within range of min and max.
     *  @param {number} [min=0] - Integer specifying bottom of range
     *  @param {number} [max=9] - Integer specifying top of range
     *  @return {number} Random integer
     */

    getRndInt(min = 0, max = 9) {
        //console.log(`util.getRndInt(${min}, ${max})`);
        return Math.floor(Math.random() * (max - min + 1) + min);
    },


    /**
     *  Removes HTML tags and comments from a string.
     *  @param {string} string
     *  @return {string} Sanitized string
     */

    removeHtmlTags(string) {
        //console.log(`util.removeHtmlTags(${string})`);
        const tagBody = '(?:[^"\'>]|"[^"]*"|\'[^\']*\')*',
            tagOrComment = new RegExp('<(?:' +
                '!--(?:(?:-*[^->])*--+|-?)' +  // Comment body
                '|script\\b' + tagBody + '>[\\s\\S]*?</script\\s*' +
                '|style\\b' + tagBody + '>[\\s\\S]*?</style\\s*' +
                '|/?[a-z]' +  // Regular name
                tagBody + ')>', 'gi');

        let oldString;
        do {
            oldString = string;
            string = string.replace(tagOrComment, '');
        } while (string !== oldString);
        return string.replace(/</g, '');
    },


    /**
     *  Constructs a new Timer or manipulates an existing Timer.
     *  A new Timer must be started with the 'start' method.
     *  @constructor {Object} Timer
     *  @param {number} delay - Milliseconds to delay
     *  @param {function} callback - Called after delay
     */

    Timer(delay, callback) {
        //console.log(`util.Timer(${delay})`);
        var id, started, running,
            remaining = delay;

        this.start = function () {
            running = true;
            started = new Date();
            id = setTimeout(callback, remaining);
        };
        this.pause = function () {
            running = false;
            clearTimeout(id);
            remaining -= new Date() - started;
        };
        this.timeLeft = function () {
            if (running) {
                this.pause();
                this.start();
            }
            return remaining;
        };
        this.timePassed = function () {
            if (running) {
                this.pause();
                this.start();
            }
            return delay - remaining;
        };
        this.state = function () {
            return running;
        };
    }
};
