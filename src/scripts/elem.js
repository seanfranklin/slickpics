/* globals $ */
'use strict';

const elem = {

    gifStyle:            $('#gif-style'),

    layers: {
        pic1:            $('#pic-1'),
        pic2:            $('#pic-2'),
        pic3:            $('#pic-3'),
        opts:            $('#opts'),
        ctrls:           $('#ctrls')
    },

    ctrls: {
        prev:            $('#prev'),
        pause:           $('#pause'),
        next:            $('#next'),
        search:          $('#search'),
        searchTags:      $('#search-tags'),
        opts:            $('#show-opts'),
        fullscreen:      $('#fullscreen')
    },
        
    picInfo: {
        pic1: {
            titleAuthor: $('#pic-1 .picinfo-title-author'),
            title:       $('#pic-1 .picinfo-title'),
            author:      $('#pic-1 .picinfo-author'),
            geo:         $('#pic-1 .picinfo-geo'),
            tags:        $('#pic-1 .picinfo-tags'),
            date:        $('#pic-1 .picinfo-date')
        },

        pic2: {
            titleAuthor: $('#pic-2 .picinfo-title-author'),
            title:       $('#pic-2 .picinfo-title'),
            author:      $('#pic-2 .picinfo-author'),
            geo:         $('#pic-2 .picinfo-geo'),
            tags:        $('#pic-2 .picinfo-tags'),
            date:        $('#pic-2 .picinfo-date')
        },

        pic3: {
            titleAuthor: $('#pic-3 .picinfo-title-author'),
            title:       $('#pic-3 .picinfo-title'),
            author:      $('#pic-3 .picinfo-author'),
            geo:         $('#pic-3 .picinfo-geo'),
            tags:        $('#pic-3 .picinfo-tags'),
            date:        $('#pic-3 .picinfo-date')
        },

        pic1Overlay:     $('#pic-1 .picinfo'),
        pic2Overlay:     $('#pic-2 .picinfo'),
        pic3Overlay:     $('#pic-3 .picinfo')
    },

    opts: {
        sources: {
            buttons:     $('#sources-buttons button'),
            browse:      $('#sources-browse'),
            files:       $('#sources-files'),
            filelist:    $('#sources-filelist')
        },

        filter: {
            tags:        $('#filter-tags'),
            geo:         $('#filter-geo'),
            from:        $('#filter-from'),
            to:          $('#filter-to'),
            safe:        $('#filter-safe')
        },

        display: {
            change:      $('#display-change'),
            pause:       $('#display-pause'),
            transition:  $('#display-transition')
        },

        picInfo: {
            title:       $('#opt-picinfo-title'),
            author:      $('#opt-picinfo-author'),
            geo:         $('#opt-picinfo-geo'),
            tags:        $('#opt-picinfo-tags'),
            date:        $('#opt-picinfo-date'),
            show:        $('#opt-picinfo-show')
        },

        ctrls: {
            hide:        $('#ctrls-hide'),
            position:    $('#ctrls-position')
        },

        nav:             $('#opts-nav'),
        tips:            $('.opt-tip')
    }

};
