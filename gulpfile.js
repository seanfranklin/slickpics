'use strict';

const gulp = require('gulp'),
    concat = require('gulp-concat'),
   postcss = require('gulp-postcss'),
      sass = require('gulp-sass'),
sourcemaps = require('gulp-sourcemaps'),
    terser = require('gulp-terser'),
  csswring = require('csswring');

gulp.task('styles', function () {
    return sass(['src/styles/*.scss'], { sourcemap: true })
        .on('error', sass.logError)
        .pipe(postcss([csswring({ removeAllComments: true })]))
        .pipe(concat('style.min.css'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('www/'));
});

gulp.task('scripts', function () {
    const srcFiles = [
        'src/scripts/geo.js',
        'src/scripts/util.js',
        'src/scripts/a500px.js',
        'src/scripts/flickr.js',
        'src/scripts/local.js',
        'src/scripts/state.js',
        'src/scripts/elem.js',
        'src/scripts/sp.js',
        'src/scripts/events.js',
        'src/scripts/init.js'
    ];
    return gulp.src(srcFiles)
        .pipe(sourcemaps.init())
        .pipe(terser())
        .pipe(concat('script.min.js'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('www/scripts/'));
});
